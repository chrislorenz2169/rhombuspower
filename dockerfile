#Start from amazonlinux 2

FROM amazonlinux:2

#install python 3 and pip

RUN yum install -y python3 python3-pip

#install AWS and data libraries

run python3 -m pip install boto3 pandas s3fs

#Copy application

#COPY src/code

#Start from application

#ENTRYPOINT["python3", "/code/main.py"]
